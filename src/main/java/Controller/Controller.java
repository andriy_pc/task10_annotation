package Controller;

import Model.Model;
import Viewer.Display;
import Viewer.View;
import units.TestClass;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class Controller {
    private Model model;
    private View viewer;
    private Map<Integer, Display> functions;
    private Scanner in;

    public Controller(Model m, View v) {
        model = m;
        viewer = v;
        in = new Scanner(System.in);
        functions = new LinkedHashMap<>();
        init();
    }

    private void init() {
        functions.put(0, this::quit);
        functions.put(1, this::printAnnotatedFields);
        functions.put(2, this::printAnnotationValue);
        functions.put(3, this::invokeThreeMethods);
        functions.put(4, this::setValue);
        functions.put(5, this::invokeNonFixedArguments);
        functions.put(6, this::printAboutUnknown);
        functions.put(7, this::changeLocale);
    }

    private void quit() {
        System.out.println("Quit...");
        System.exit(0);
    }
    private void printAnnotatedFields() {
        model.printFieldsWithAnnotation();
        System.out.println();
    }
    private void printAnnotationValue() {
        model.printAnnotationValue();
        System.out.println();
    }
    private void invokeThreeMethods() throws InvocationTargetException, IllegalAccessException {
        model.invokeMethods();
        System.out.println();
    }
    private void setValue() throws IllegalAccessException {
        model.setValueOfField();
        System.out.println();
    }
    private void invokeNonFixedArguments()
            throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        model.invokeNonFixedArguments();
        System.out.println();
    }
    private void printAboutUnknown() {
        model.printInformation(new TestClass().getClass());
        System.out.println();
    }

    private void changeLocale() {
        viewer.initLocale();
    }
    public void run()
            throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        System.out.println("Please select option:");
        viewer.display();
        int choice;
        choice = in.nextInt();
        functions.get(choice).display();
    }
}
