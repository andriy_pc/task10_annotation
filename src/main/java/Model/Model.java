package Model;

import units.MyAnnotation;
import units.MyMethod;
import units.TestClass;

import java.util.Random;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class Model {
    private Random rand;
    private TestClass test;
    private Class clazz;
    private Field[] fields;
    private Method[] methods;

    public Model() {
        rand = new Random(47);
        test = new TestClass();
        clazz = test.getClass();
        fields = clazz.getDeclaredFields();
        methods = clazz.getDeclaredMethods();
    }

    public void printFieldsWithAnnotation() {
        fields = clazz.getDeclaredFields();
        for (Field f : fields) {
            if (f.getAnnotation(MyAnnotation.class) != null) {
                System.out.println(f.getName());
            }
        }
    }
    public void printAnnotationValue() {
        for (Field f : fields) {
            if (f.isAnnotationPresent(MyAnnotation.class)) {
                MyAnnotation ma = f.getAnnotation(MyAnnotation.class);
                System.out.println("field: " + f.getName() +
                        "\ntype: " + ma.type() + ";\nvalue: "+ ma.value());
            }
        }
    }
    public void invokeMethods() throws InvocationTargetException, IllegalAccessException {
        Class[] param;
        for(Method m : methods) {
            if(m.getAnnotation(MyMethod.class) != null) {
                param = m.getParameterTypes();
                if (param.length == 0) {
                    m.invoke(test);
                } else if (param.length == 1) {
                    m.invoke(test, 5);
                } else if (param.length == 2) {
                    m.invoke(test, 5, 'c');
                }
            }
        }
    }
    public void setValueOfField() throws IllegalAccessException {
        Field field = fields[rand.nextInt(3)];
        field.setAccessible(true);
        if(field.getType() == int.class) {
            field.setInt(test, 5);
        } else if(field.getType() == double.class) {
            field.setDouble(test, 3.14);
        } else if(field.getType() == String.class) {
            field.set(test, "Door");
        }
        System.out.println("Field value was set to: " + field.get(test));
    }
    public void invokeNonFixedArguments()
            throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        Class[] param;
        String[] strParams = new String[] {};
        int[] intParams = new int[] {1, 2, 3};
        Method m1 = clazz.getDeclaredMethod("myMethod1", String[].class);
        Method m2 = clazz.getDeclaredMethod("myMethod2", String.class, int[].class);
        m1.invoke(test, (Object) strParams);
        m2.invoke(test, "okko", intParams);

    }
    public <T> void printInformation(Class<T> unknownClass) {
        System.out.println(unknownClass.getName());
        System.out.println("Fields:");
        for(Field f : unknownClass.getDeclaredFields()) {
            System.out.println(f.getType() + " " + f.getName());
        }
        System.out.println("Methods:");
        for(Method m : unknownClass.getDeclaredMethods()) {
            System.out.println(m.getReturnType() + ": " + m.getName());
        }
    }
}
