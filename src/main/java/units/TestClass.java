package units;

public class TestClass {
    public TestClass() {

    }
    @MyAnnotation(type="users", value=-1)
    private String firstField = "firstField";
    @MyAnnotation(value=15)
    protected double secondField = 15;
    @MyAnnotation
    public int thirdField = 1;
    char fourthField = 'a';
    private double fieldToSetValue;

    public static void myMethod2(String a, int... args) {
        System.out.println("myMethod(String a, int... args)");
    }
    public static void myMethod1(String... args) {
        System.out.println("myMethod(String... args)");
    }
    @MyMethod
    public void firstMethod() {
        System.out.println("firstMethod");
        return;
    }
    @MyMethod
    public int secondMethod(int i) {
        System.out.println("secondMethod");
        return i;
    }
    @MyMethod
    public char thirdMethod(int i, char c) {
        System.out.println("thirdMethod");
        return (char)(c+i);
    }
}
