package units;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value= RetentionPolicy.RUNTIME)
@Target(value=ElementType.FIELD)
public @interface MyAnnotation {
    String type() default "default";
    int value() default 1;
}
