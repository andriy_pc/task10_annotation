package Viewer;

import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

public class View {
    private Locale enLocale;
    private Locale uaLocale;
    private ResourceBundle bundle;
    private boolean isUaLocale = false;
    private ArrayList<String> menu;

    public View() {
        enLocale = new Locale("en", "ENGLAND");
        uaLocale = new Locale("ua", "Ukraine");
        initLocale();
    }

    private void init(Locale locale) {
        menu = new ArrayList<>();
        bundle = ResourceBundle.getBundle("menu", locale);
        menu.add(bundle.getString("0"));
        menu.add(bundle.getString("1"));
        menu.add(bundle.getString("2"));
        menu.add(bundle.getString("3"));
        menu.add(bundle.getString("4"));
        menu.add(bundle.getString("5"));
        menu.add(bundle.getString("6"));
        menu.add(bundle.getString("7"));
    }

    public void display() {
        for(String s : menu) {
            System.out.println(s);
        }
    }

    public void initLocale() {
        if(isUaLocale) {
            init(enLocale);
            isUaLocale = false;
            return;
        }
        init(uaLocale);
        isUaLocale = true;
    }

}
