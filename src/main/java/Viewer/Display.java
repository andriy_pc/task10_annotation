package Viewer;

import java.lang.reflect.InvocationTargetException;

public interface Display {
    void display()
            throws InvocationTargetException, IllegalAccessException, NoSuchMethodException;
}
