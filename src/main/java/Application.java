
import Controller.Controller;
import Model.Model;
import Viewer.View;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Application {
    private static Logger log = LogManager.getLogger();
    public static void main(String[] args) {
        Controller c = new Controller(new Model(), new View());
        try {
            while(true) {
                c.run();
            }
        } catch (Exception e) {
            log.fatal("Exception were thrown");
            StackTraceElement[] ste = e.getStackTrace();
            for(StackTraceElement s : ste) {
                log.fatal(s);
            }
            throw new RuntimeException();
        }

    }
}
